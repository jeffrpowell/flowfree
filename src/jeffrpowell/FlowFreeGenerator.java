package jeffrpowell;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FlowFreeGenerator
{

    static final int SIZE = 10;
    static final Random RANDOM = new Random();

    public static void main(String[] args)
    {
	Grid grid = new Grid();
	System.out.println("Generating grid....");
	boolean success = grid.generate();
	while (!success)
	{
	    System.out.println("trying again...");
	    success = grid.generate();
	}
	System.out.println(grid);
    }

    private static class Grid
    {

	Map<Character, List<Point2D>> paths;
	Set<Point2D> claimedPoints;

	public Grid()
	{
	    paths = new HashMap<>();
	    claimedPoints = new HashSet<>();
	}

	public boolean generate()
	{
	    reset();
	    char currentSymbol = 'a';
	    for (int i = 0; i < SIZE * 2; i++)
	    {
		paths.put(currentSymbol, new ArrayList<>());
		Point2D startPt = generateStartPoint();
		paths.get(currentSymbol).add(startPt);
		claimedPoints.add(startPt);
		currentSymbol++;
	    }
	    boolean stateChanged = true;
	    while (stateChanged)
	    {
		stateChanged = false;
		for (List<Point2D> path : paths.values())
		{
		    stateChanged = performWalk(path);
		}
	    }
	    for (int col = 0; col < SIZE; col++)
	    {
		for (int row = 0; row < SIZE; row++)
		{
		    Point2D sut = new Point2D.Double(col, row);
		    if (!claimedPoints.contains(sut))
		    {
			if (currentSymbol > 'z')
			{
			    System.out.print("Too many symbols; ");
			    return false;
			}
			List<Point2D> newPath = new ArrayList<>();
			paths.put(currentSymbol, newPath);
			newPath.add(sut);
			claimedPoints.add(sut);
			while(performWalk(newPath)) {}
			if (newPath.size() < 3)
			{
			    System.out.print("Small pocket generated; ");
//			    return false;
			}
			currentSymbol++;
		    }
		}
	    }
	    return true;
	}

	private Point2D generateStartPoint()
	{
	    Point2D startPt;
	    while (true)
	    {
		startPt = new Point2D.Double(RANDOM.nextInt(SIZE), RANDOM.nextInt(SIZE));
		if (!claimedPoints.contains(startPt))
		{
		    return startPt;
		}
	    }
	}

	private boolean performWalk(List<Point2D> currentPath)
	{
	    boolean stateChanged = false;
	    int limit = RANDOM.nextInt(3) + 1;
	    for (int i = 0; i < limit; i++)
	    {
		List<Point2D> neighbors = getOpenNeighbors(currentPath, true);
		if (neighbors.isEmpty())
		{
		    continue;
		}
		
		Point2D claimedPt = neighbors.get(RANDOM.nextInt(neighbors.size()));
		claimedPoints.add(claimedPt);
		currentPath.add(claimedPt);
		stateChanged = true;
	    }
	    return stateChanged;
	}

	private List<Point2D> getOpenNeighbors(List<Point2D> pts, boolean performPocketCheck)
	{
	    Point2D pt = pts.remove(pts.size() - 1);
	    List<Point2D> neighbors = getPossibleNeighbors(pt)
		.filter(this::isPointAvailable)
		.filter(neighbor -> !getPossibleNeighbors(neighbor).anyMatch(pts::contains))
		.filter(neighbor -> !performPocketCheck || doesNotCreatePocket(neighbor))
		.collect(Collectors.toList());
	    pts.add(pt);
	    return neighbors;
	}
	
	private boolean doesNotCreatePocket(Point2D potentialNeighbor)
	{
	    return getOpenNeighbors(potentialNeighbor).stream().allMatch(secondNeighbor -> {
		List<Point2D> tempPath = new ArrayList<>();
		tempPath.add(potentialNeighbor);
		tempPath.add(secondNeighbor);
		return !getOpenNeighbors(tempPath, false).isEmpty();
	    });
	}

	private List<Point2D> getOpenNeighbors(Point2D pt)
	{
	    return getOpenNeighbors(Stream.of(pt).collect(Collectors.toList()), false);
	}
	
	private Stream<Point2D> getPossibleNeighbors(Point2D pt)
	{
	    return Stream.of(
		new Point2D.Double(pt.getX() - 1, pt.getY()),
		new Point2D.Double(pt.getX(), pt.getY() - 1),
		new Point2D.Double(pt.getX() + 1, pt.getY()),
		new Point2D.Double(pt.getX(), pt.getY() + 1)
	    );
	}

	private boolean isPointAvailable(Point2D pt)
	{
	    return pt.getX() >= 0
		&& pt.getX() < SIZE
		&& pt.getY() >= 0
		&& pt.getY() < SIZE
		&& !claimedPoints.contains(pt);
	}

	private void reset()
	{
	    paths.clear();
	    claimedPoints.clear();
	}

	@Override
	public String toString()
	{
	    HashMap<Point2D, Character> ptToCharMap = paths.entrySet().stream()
		.collect(
		    HashMap::new, 
		    (map, entry) -> {
			for (Point2D pt : entry.getValue())
			{
			    map.put(pt, entry.getKey());
			}
		    },
		    Map::putAll
		);
	    StringBuilder builder = new StringBuilder(SIZE).append("\n");
	    for (int col = 0; col < SIZE; col++)
	    {
		for (int row = 0; row < SIZE; row++)
		{
		    builder.append(ptToCharMap.get(new Point2D.Double(col, row)));
		}
		builder.append("\n");
	    }
	    return builder.toString();
	}
    }
}
