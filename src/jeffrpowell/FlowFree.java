package jeffrpowell;

import java.awt.geom.Point2D;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

public class FlowFree
{

    public static void main(String[] args)
    {
	Map<Character, List<Point2D>> symbolPairLocations = new HashMap<>();
	
	Scanner in = new Scanner(System.in);
	int size = in.nextInt();
	for (int rowIndex = 0; rowIndex < size; rowIndex++)
	{
	    String row = in.nextLine();
	    for (int i = 0; i < row.length(); i++)
	    {
		char symbol = row.charAt(i);
		if (symbol == '.')
		{
		    continue;
		}
		if (!symbolPairLocations.containsKey(symbol))
		{
		    symbolPairLocations.put(symbol, new ArrayList<>());
		}
		symbolPairLocations.get(symbol).add(new Point2D.Double(rowIndex, i));
	    }
	}
	
	BacktraceFlowSolver solver = new BacktraceFlowSolver(symbolPairLocations, size, System.out);
	solver.outputSolution();
    }
    
    private static class BacktraceFlowSolver
    {
	private final Map<Character, List<Point2D>> symbolPairLocations;
	private final int gridSize;
	private final PrintStream out;
	private boolean solutionFound;

	public BacktraceFlowSolver(Map<Character, List<Point2D>> symbolPairLocations, int gridSize, PrintStream out)
	{
	    this.symbolPairLocations = symbolPairLocations;
	    this.gridSize = gridSize;
	    this.out = out;
	    this.solutionFound = false;
	}
	
	public void outputSolution()
	{
	    backtrace();
	}
	
	private void backtrace()
	{
	    if (reject() || accept())
	    {
		return;
	    }
	    do
	    {
		generate();
		backtrace();
	    }
	    while (!solutionFound);
	}
	
	private boolean reject()
	{
	    return false;
	}
	
	private boolean accept()
	{
	    //if everything looks good
	    solutionFound = true;
	    //print solution
	    return true;
	}
	
	private void generate()
	{
	    
	}
    }
    
    private static class Grid
    {
	private final int size;
	private final Set<Point2D> boundaryPoints;
	private final Map<Character, Path> paths;

	public Grid(int size)
	{
	    this.size = size;
	    this.boundaryPoints = new HashSet<>();
	    this.paths = new HashMap<>();
	}
	
	
    }
    
    private static class Path
    {
	Stack<Point2D> tailA;
	Stack<Point2D> tailB;
	
	public Path(Point2D ptA, Point2D ptB)
	{
	    tailA = new Stack<>();
	    tailB = new Stack<>();
	    tailA.push(ptA);
	    tailB.push(ptB);
	}
    }
}
